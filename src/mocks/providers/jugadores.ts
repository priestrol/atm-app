import { Injectable } from "@angular/core";

import { Jugador } from "../../models/jugador";

@Injectable()
export class Jugadores {
  jugadores: Jugador[] = [];
  juveniles: Jugador[] = [];
  cadetes: Jugador[] = [];
  infantiles: Jugador[] = [];
  alevines: Jugador[] = [];

  defaultItem: any = {
    name: "Ejemplo Ejemplo",
    profilePic: "assets/img/jugadores/cris.jpg",
    categoria: "Ejemplo"
  };

  constructor() {
    //AÑADIR CAMPOS PARA ESCUELA, ENTRENOS, ETC
    let jugadores = [
      {
        name: "Antonio Lopez Ruiz",
        profilePic: "assets/img/jugadores/benzema.jpg",
        categoria: "Alevin"
      },
      {
        name: "Jose Luis Aranda Estevez",
        profilePic: "assets/img/jugadores/buffon.jpg",
        categoria: "Cadete"
      },
      {
        name: "Pablo Ruiz Iñigo",
        profilePic: "assets/img/jugadores/cris.jpg",
        categoria: "Infantil"
      },
      {
        name: "Francisco M. Roldán Pérez",
        profilePic: "assets/img/jugadores/luis.jpg",
        categoria: "Juvenil"
      },
      {
        name: "Pablo Prieto dos Santos Aveiro",
        profilePic: "assets/img/jugadores/pintocasillas.jpg",
        categoria: "Juvenil"
      },
      {
        name: "Jesus Estevez Rodriguez",
        profilePic: "assets/img/jugadores/pique.jpg",
        categoria: "Infantil"
      },
      {
        name: "Jose Maria Malagon Martinez",
        profilePic: "assets/img/jugadores/ribery.jpg",
        categoria: "Alevin"
      },
      {
        name: "Andres Rubio Rocha",
        profilePic: "assets/img/jugadores/robben.jpg",
        categoria: "Alevin"
      },
      {
        name: "Raul Puente Bañon",
        profilePic: "assets/img/jugadores/benzema.jpg",
        categoria: "Alevin"
      },
      {
        name: "Mariano Carrasco de las Heras",
        profilePic: "assets/img/jugadores/buffon.jpg",
        categoria: "Cadete"
      },
      {
        name: "Javier Hermoso Cozar",
        profilePic: "assets/img/jugadores/cris.jpg",
        categoria: "Infantil"
      },
      {
        name: "Alfonso Fresno Carnero",
        profilePic: "assets/img/jugadores/luis.jpg",
        categoria: "Juvenil"
      },
      {
        name: "Hugo Suarez Borreguero",
        profilePic: "assets/img/jugadores/pintocasillas.jpg",
        categoria: "Juvenil"
      },
      {
        name: "Jose Ramon Moraleda Bilbao",
        profilePic: "assets/img/jugadores/pique.jpg",
        categoria: "Infantil"
      },
      {
        name: "David Carvajal del Pozo",
        profilePic: "assets/img/jugadores/ribery.jpg",
        categoria: "Alevin"
      },
      {
        name: "Angel Medina Galera",
        profilePic: "assets/img/jugadores/robben.jpg",
        categoria: "Alevin"
      }
    ];

    for (let jugador of jugadores) {
      this.jugadores.push(new Jugador(jugador));
      switch (jugador.categoria) {
        case "Juvenil":
          this.juveniles.push(new Jugador(jugador));
          break;
        case "Cadete":
          this.cadetes.push(new Jugador(jugador));
          break;
        case "Infantil":
          this.infantiles.push(new Jugador(jugador));
          break;
        case "Alevin":
          this.alevines.push(new Jugador(jugador));
          break;
      }
    }
  }

  filterItems(searchTerm) {
    return this.jugadores.filter(item => {
      return item.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }

  query(params?: any) {
    if (!params) {
      return this.jugadores;
    }

    return this.jugadores.filter(jugador => {
      for (let key in params) {
        let field = jugador[key];
        if (
          typeof field == "string" &&
          field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0
        ) {
          return jugador;
        } else if (field == params[key]) {
          return jugador;
        }
      }
      return null;
    });
  }

  add(jugador: Jugador) {
    this.jugadores.push(jugador);
  }

  delete(jugador: Jugador) {
    this.jugadores.splice(this.jugadores.indexOf(jugador), 1);
  }
}
