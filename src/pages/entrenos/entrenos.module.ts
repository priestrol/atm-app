import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EntrenosPage } from './entrenos';

@NgModule({
  declarations: [
    EntrenosPage,
  ],
  imports: [
    IonicPageModule.forChild(EntrenosPage),
  ],
})
export class EntrenosPageModule {}
