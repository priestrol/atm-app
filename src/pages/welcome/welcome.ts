import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  MenuController,
  ToastController
} from "ionic-angular";
import { User } from "../../providers";
import { TranslateService } from "@ngx-translate/core";
import { MainPage } from "../";

@IonicPage()
@Component({
  selector: "page-welcome",
  templateUrl: "welcome.html"
})
export class WelcomePage {
  account: { username: string; password: string } = {
    username: "",
    password: ""
  };

  constructor(
    public navCtrl: NavController,
    public menu: MenuController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService
  ) {
    this.menu.enable(false);
  }

  ionViewDidEnter() {
    //PARA ASEGURARNOS DE QUE NO PUEDES USAR EL MENU ANTES DE INICIAR SESION
    //(metodo de ciclo de vida que se ejecuta cada vez que se entre en esta pagina)
    //**posibilidad de que inicien sesion y luego la cierren y vuelvan aqui**
    this.menu.enable(false);
  }

  doLogin() {
    this.user.login(this.account).subscribe(
      resp => {
        this.navCtrl.push(MainPage);
        this.menu.enable(true);
      },
      err => {
        // ----TESTING----  SIEMPRE INICIAS SESION   --ARREGLAR AL INSERTAR LOGICA
        this.navCtrl.push(MainPage);
        console.log(err);
        this.menu.enable(true);

        // MENSAJITO PARA CUANDO SALTE EL CATCH
        // let toast = this.toastCtrl.create({
        //   message: "No ha sido posile iniciar sesion, usuario o contraseña incorrecta",
        //   duration: 3000,
        //   position: "top"
        // });
        // toast.present();
      }
    );
  }
}
