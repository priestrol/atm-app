import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SancionesPage } from './sanciones';

@NgModule({
  declarations: [
    SancionesPage,
  ],
  imports: [
    IonicPageModule.forChild(SancionesPage),
  ],
})
export class SancionesPageModule {}
