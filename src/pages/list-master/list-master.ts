import { Component } from "@angular/core";
import {
  IonicPage,
  ModalController,
  NavController,
  MenuController
} from "ionic-angular";

import { Jugador } from "../../models/jugador";
import { Jugadores } from "../../providers";

interface PageItem {
  title: string;
  img: string;
  WidhtHeight: number;
  component: any;
}
type PageList = PageItem[];

@IonicPage()
@Component({
  selector: "page-list-master",
  templateUrl: "list-master.html"
})
export class ListMasterPage {
  currentItems: Jugador[];

  pages: PageList;

  constructor(
    public navCtrl: NavController,
    public jugadores: Jugadores,
    public modalCtrl: ModalController,
    public menu: MenuController
  ) {
    this.currentItems = this.jugadores.query();

    // mientras no tengamos una base de datos nos comemos los mocos
    // y tenemos que meter la informacion a pelo :v
    this.pages = [
      {
        title: "Estudios",
        img: "assets/img/iconos/open-book.png",
        WidhtHeight: 60,
        component: "EstudiosPage"
      },
      {
        title: "Entrenos",
        img: "assets/img/iconos/soccer.png",
        WidhtHeight: 60,
        component: "EntrenosPage"
      },
      {
        title: "Dieta",
        img: "assets/img/iconos/diet.png",
        WidhtHeight: 60,
        component: "DietaPage"
      },
      {
        title: "Biblioteca",
        img: "assets/img/iconos/books.png",
        WidhtHeight: 60,
        component: "BibliotecaPage"
      },
      {
        title: "Viajes",
        img: "assets/img/iconos/bus.png",
        WidhtHeight: 60,
        component: "ViajesPage"
      },
      {
        title: "Información",
        img: "assets/img/iconos/info.png",
        WidhtHeight: 60,
        component: "InformacionPage"
      },
      {
        title: "Sanciones",
        img: "assets/img/iconos/red.png",
        WidhtHeight: 60,
        component: "SancionesPage"
      },
      {
        title: "Partidos",
        img: "assets/img/iconos/partidos.png",
        WidhtHeight: 60,
        component: "PartidosPage"
      }
    ];
  }

  ionViewDidEnter() {
    this.menu.enable(true);
  }

  openPage(page: PageItem) {
    this.navCtrl.push(page.component);
  }
}
