import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";

import { Jugadores } from "../../providers";

@IonicPage()
@Component({
  selector: "page-item-detail",
  templateUrl: "item-detail.html"
})
export class ItemDetailPage {
  jugador: any;

  constructor(
    public navCtrl: NavController,
    navParams: NavParams,
    jugadores: Jugadores
  ) {
    this.jugador = navParams.get("jugador");
  }
}
