import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController
} from "ionic-angular";

import { Jugador } from "../../models/jugador";
import { Jugadores } from "../../providers";

@IonicPage()
@Component({
  selector: "page-search",
  templateUrl: "search.html"
})
export class SearchPage {
  currentItems: any = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public jugadores: Jugadores,
    public modalCtrl: ModalController
  ) {
    this.currentItems = this.jugadores.query();
    console.log(this.currentItems);
  }

  getItems(ev) {
    let val = ev.target.value;
    this.currentItems = this.jugadores.query({ name: val });
  }

  filterItemsOfType(type) {
    return this.currentItems.filter(x => x.categoria == type);
  }

  openItem(jugador: Jugador) {
    this.navCtrl.push("ItemDetailPage", {
      jugador: jugador
    });
  }

  addItem() {
    let addModal = this.modalCtrl.create("ItemCreatePage");
    addModal.onDidDismiss(jugador => {
      if (jugador) {
        this.jugadores.add(jugador);
      }
    });
    addModal.present();
  }

  deleteItem(jugador) {
    this.jugadores.delete(jugador);
  }
}
