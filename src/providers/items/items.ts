import { Injectable } from "@angular/core";

import { Jugador } from "../../models/jugador";
import { Api } from "../api/api";

@Injectable()
export class Items {
  constructor(public api: Api) {}

  query(params?: any) {
    return this.api.get("/items", params);
  }

  add(item: Jugador) {}

  delete(item: Jugador) {}
}
